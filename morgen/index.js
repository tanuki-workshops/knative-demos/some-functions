module.exports = (param) => {
  let data = typeof param == 'object' ? param : {}
  let name = data.name == null ? "Erika Mustermann" : data.name
  return `${process.env.FLAG} Morgen ${name}`
}
