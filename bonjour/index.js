module.exports = (param) => {
  let data = typeof param == 'object' ? param : {}
  let name = data.name == null ? "Jean Dupont" : data.name
  return `${process.env.FLAG} Bonjour ${name}`
}
